# weavlo.com
This website is used to share Weavlo information over the Web, including distributable binary packages.

## Attributions
Weavlo.com was written using Supercharge: an open-source Node.js framework by Marcus Poehls.
*  https://superchargejs.com
*  https://github.com/superchargejs/supercharge

Follow <a href="http://twitter.com/marcuspoehls">@marcuspoehls</a> and <a href="http://twitter.com/superchargejs">@superchargejs</a> on Twitter for updates!</em>

## Quick Start
This website runs on Node.js (Hapi.js + Superchargejs)

```
git clone https://gitlab.com/weavlo/weavlo.com.git
npm install
node start
```

## License
The weavlo.com code is [MIT licensed](https://gitlab.com/weavlo/weavlo.com/blob/master/LICENSE).