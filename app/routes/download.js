'use strict'

module.exports = {
  method: 'GET',
  path: '/download',
  handler: async (_, h) => {
    const Fs = require('@supercharge/framework/filesystem')

    // Fetch a list of files in '../public/weavlo_packages'
    try {
      const weavlo_package_files = await Fs.files('public/weavlo_packages')
      console.log(weavlo_package_files)
      return h.view('download', { weavlo_package_files }, { layout: 'app' })
    } catch (error) {
      console.error('Weavlo found an error:\n', error)
      // For the user, fail gracefully by returning the view (with no files).
      const weavlo_package_files = []
      return h.view('download', { weavlo_package_files }, { layout: 'app' })  
    }
  }
}
 
